#include <pebble.h>
#include <string.h>
static Window *s_main_window;
static AppSync s_sync;
static MenuLayer *s_menu_layer;
char *movies[5];
char *gross[5];

enum Chart {
  ONE = 0x0,
  TWO = 0x1,
  THREE = 0x2,
  FOUR = 0x3,
  FIVE = 0x4
};

static uint16_t menu_get_num_sections_callback(MenuLayer *menu_layer, void *data) {
  return 1;
}

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return 5;
}

static int16_t menu_get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

static void menu_draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data) {
  // Draw title text in the section header
 menu_cell_basic_header_draw(ctx, cell_layer, "Box Office");
}

static void menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data) {
  switch (cell_index->row) {
    case 0:
      menu_cell_basic_draw(ctx, cell_layer, movies[0], gross[0], NULL);
      break;

    case 1:
      menu_cell_basic_draw(ctx, cell_layer, movies[1], gross[1], NULL);
      break;

    case 2:
      menu_cell_basic_draw(ctx, cell_layer, movies[2], gross[2], NULL);
      break;

    case 3:
      menu_cell_basic_draw(ctx, cell_layer, movies[3], gross[3], NULL);
      break;

    case 4:
      menu_cell_basic_draw(ctx, cell_layer, movies[4], gross[4], NULL);

      break;
  }

}

static void inbox_received_callback(DictionaryIterator *iterator, void *context) {
    Tuple *t=dict_read_first(iterator);
    int i=0;
    while (t != NULL) {
      char *tmp=t->value->cstring;
      int length=0;
      while(tmp[length]!='\n' && length<(int) strlen(tmp)){
        length++;
      }


      movies[i]=tmp;

      movies[i][length]='\0';
      gross[i]=tmp+length+1;
      i++;
      t = dict_read_next(iterator);
    }
    menu_layer_reload_data(s_menu_layer);

  }

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);

  GRect bounds = layer_get_bounds(window_layer);
  s_menu_layer=menu_layer_create(bounds);
  menu_layer_set_click_config_onto_window(s_menu_layer, window);
  menu_layer_set_callbacks(s_menu_layer,NULL,(MenuLayerCallbacks){
    .get_num_sections=menu_get_num_sections_callback,
    .get_num_rows=menu_get_num_rows_callback,
    .get_header_height = menu_get_header_height_callback,
    .draw_header = menu_draw_header_callback,
    .draw_row = menu_draw_row_callback,
  });
  #ifdef PBL_COLOR
    menu_layer_set_normal_colors(s_menu_layer, GColorDarkCandyAppleRed, GColorBlack);
  #endif
  menu_layer_set_click_config_onto_window(s_menu_layer, window);
  layer_add_child(window_layer,menu_layer_get_layer(s_menu_layer));
}

static void window_unload(Window *window) {
  menu_layer_destroy(s_menu_layer);
}

void init(){
  app_message_register_inbox_received(inbox_received_callback);
  app_message_open(app_message_inbox_size_maximum(), app_message_outbox_size_maximum());
  s_main_window=window_create();
  #ifdef PBL_COLOR
    window_set_background_color(s_main_window, GColorDarkCandyAppleRed);
  #else
   window_set_background_color(s_main_window, GColorWhite);
  #endif
  window_set_window_handlers(s_main_window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload
  });
  window_stack_push(s_main_window, true);

}

void deinit(){
  window_destroy(s_main_window);
  app_sync_deinit(&s_sync);
}

int main(){
  init();
  app_event_loop();
  deinit();
}
